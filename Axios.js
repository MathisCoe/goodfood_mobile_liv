import axios from 'axios';
const URL = require('./URL');
export default axios.create({
  baseURL: URL.URL+`api/`
});
