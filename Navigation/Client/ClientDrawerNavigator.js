import React from "react";
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from "@react-navigation/drawer";

import Accueil from "../../Views/Accueil";
import ClientBottomsTabs from './ClientBottomsTabs';
import Deconnexion from "../../Views/Deconnexion";
const Drawer = createDrawerNavigator();

const ClientDrawerNavigator = () => {
  return (
    <NavigationContainer>
    <Drawer.Navigator
    drawerType={'slide'}
    drawerContentOptions={{
      activeTintColor: '#FFC853',
      activeBackgroundColor: 'black',
      inactiveTintColor: 'black',
      inactiveBackgroundColor: '#FFC853',
      labelStyle:{
        marginLeft:5
      }
    }}
    >
    <Drawer.Screen name="Accueil" component={ClientBottomsTabs} />
    <Drawer.Screen name="????" component={Accueil} />
    <Drawer.Screen name="Deconnexion" component={Deconnexion} />
    </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default ClientDrawerNavigator;
