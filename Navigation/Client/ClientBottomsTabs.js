import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Image} from 'react-native';
import Accueil from "../../Views/Accueil";
import Panier from "../../Views/Panier";
import RestaurantsNavigation from "./RestaurantsNavigation";
import ProfilNavigation from "./ProfilNavigation";

const Tab = createBottomTabNavigator ();

const ClientBottomsTabs = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator screenOptions={{
          headerShown: false
        }}>
        <Tab.Screen name="Accueil" component={Accueil} 
        options={{tabBarIcon: () => (<Image source={require("../../assets/home.png")} style={{width: 20, height: 20}} />)}}/>               
        <Tab.Screen name="Liste Restaurants" component={RestaurantsNavigation} 
        options={{tabBarIcon: () => (<Image source={require("../../assets/liste.png")} style={{width: 20, height: 20}} />)}}/>
        <Tab.Screen name="Panier" component={Panier}
        options={{tabBarIcon: () => (<Image source={require("../../assets/panier.png")} style={{width: 20, height: 20}} />)}}/>
        <Tab.Screen name="Profil" component={ProfilNavigation}
        options={{tabBarIcon: () => (<Image source={require("../../assets/profil.png")} style={{width: 20, height: 20}} />)}}/>
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default ClientBottomsTabs;
