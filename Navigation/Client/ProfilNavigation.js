import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Profil from '../../Views/Profil';
import HistoriqueCommandes from '../../Views/HistoriqueCommandes';

// Mathis est un grand génie.


// Cette navigation est ce qui permet d'accèder à historique des commandes depuis Profil
const Stack = createStackNavigator();

export default class ProfilNavigation extends Component {
    constructor(props) {
    super(props);
  }

  render() {
      return (  
      <Stack.Navigator>
        <Stack.Screen name="Votre Profil" component={Profil} />
        <Stack.Screen name="HistoriqueCommandes" component={HistoriqueCommandes} />
      </Stack.Navigator>

      );
    
  }
}
