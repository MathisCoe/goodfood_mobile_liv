import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ListeRestaurants from '../../Views/ListeRestaurants';
import CarteRestaurant from '../../Views/CarteRestaurant';
import DetailMenu from '../../Views/DetailMenu';
import DetailProduit from '../../Views/DetailProduit';
//


// Cette navigation est ce qui permet d'accèder au cartes des restaurants depuis la liste des restaurants
const Stack = createStackNavigator();

export default class RestaurantsNavigation extends Component {
    constructor(props) {
    super(props);
  }

  render() {
      return (  
      <Stack.Navigator>
        <Stack.Screen name="ListeDesRestaurants" component={ListeRestaurants} />
        <Stack.Screen name="CarteRestaurant" component={CarteRestaurant} />
        <Stack.Screen name="DetailMenu" component={DetailMenu} />
        <Stack.Screen name="DetailProduit" component={DetailProduit} />
      </Stack.Navigator>

      );
    
  }
}
