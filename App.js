import 'react-native-gesture-handler';
import { StyleSheet, Text, View } from 'react-native';
import React, { useState } from 'react';
import VerifConnexion from './Views/Connexion';

export default function App() {
	const [isLoading, setLoading] = useState(true);
  return (
   <VerifConnexion/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
