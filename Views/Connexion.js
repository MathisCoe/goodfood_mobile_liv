import React, { Component } from 'react';
import {Text,View,Animated,TouchableOpacity,TextInput,Alert,Keyboard,ActivityIndicator,} from 'react-native';
import {getToken,stockerDonnee,recupDonnee, setToken} from '../API_Laravel';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from '../Styles';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ClientBottomsTabs from '../Navigation/Client/ClientBottomsTabs';

const Stack = createNativeStackNavigator();
export default class VerifConnexion extends Component {
  state = {
    email: '',
    password: '',
    grade: null,
    token: null,
  }
  constructor(props) {
    super(props);
   this.getData();
    this.fadeAnimation = new Animated.Value(0);
  }
  async getData(){
    const email = await recupDonnee("@email");
    const password = await recupDonnee("@password");
    if(email != undefined){
    const res = await getToken(email,password);
      const token = res.data.token
      this.setState({token});
      await AsyncStorage.removeItem('@token')
      await stockerDonnee('@token',token);
      this.forceUpdate();
    }else{
      const verif = 1;
      this.setState({verif});
    }
  }
  componentDidMount() {
    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 5000,
      useNativeDriver: true,
    }).start();
  }

  async connexion(email,password){
    Keyboard.dismiss()
    if(this.state.email != '' && this.state.password != ''){
      const verif = 2;
      this.setState({verif});
      const res = await getToken(email,password);
      if(res.data.error == undefined){
        const token = res.data.token
		console.log("Connecté !")
		console.log(res.data)
        setToken(token);
        this.setState({token});
        await stockerDonnee('@token',token);
        await stockerDonnee('@email',email);
        await stockerDonnee('@password',password);
        const verif = 2;
        this.setState({verif});
        this.forceUpdate();
      }else{
        const verif = 1;
        this.setState({verif});
        Alert.alert(
          "Message d'alerte",
          "Problème de mots de passe ou mail",
          [
            { text: "D'accord"}
          ],
          { cancelable: false }
        );
      }
    }
  }


  render() {
    const token = this.state.token;
    const verif = this.state.verif;
    if(verif == 1){
      return (
        <View style={styles.container}>
        <Text style={styles.logoText}>Connexion</Text>
        <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0)' placeholder="Email"
        onChangeText={email => this.setState({ email })} placeholderTextColor="gray"/>
        <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0)' placeholder="Mot de passe"
        onChangeText={password => this.setState({ password })} placeholderTextColor="gray" secureTextEntry={true}/>
        <TouchableOpacity style={styles.TouchableOpacityConnexion} onPress={() =>this.connexion(this.state.email,this.state.password)}>
        <Text style={styles.buttonConnexion}>Se Connecter</Text>
        </TouchableOpacity>
        </View>
      );
    }else{
      if(token != null){
          return(
          <ClientBottomsTabs/>
           // <ClientDrawerNavigator/>
          );
      }else{
        return(
          <View style={styles.container}>
           <ActivityIndicator size="large" color="#000000"/>
          </View>);
      }
    }
  }
}
