import React, { Component, useState } from 'react';
import { Text, View, StyleSheet, Button, TouchableOpacity,ActivityIndicator, Animated} from 'react-native';
import styles from '../Styles';

export default class DetailProduit extends Component {
  state = {
    produit: [],
  }  
    constructor(props) {
    super(props);
    const produit = this.props.route.params.produit
    this.setState({produit});
    this.fadeAnimation = new Animated.Value(0);
  }



  componentDidMount() {
    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }

  render() {
    console.log("render");
    console.log(this.props.route.params.produit);
    const produit = this.props.route.params.produit;
    if(produit != undefined){
    return (
    <View style={styles.container}>
      <Text style={styles.TextHaut}>Detail {produit.nom} </Text>
    </View>
    );
  }
  else{
    return(
      <View style={styles.containerLoading}>
      <ActivityIndicator size="large" color="#000000"/>
      </View>);
  }
  }

}
