import React, { Component, useState } from 'react';
import { Text, View, StyleSheet, Button, TouchableOpacity,FlatList, Animated,ActivityIndicator,Image} from 'react-native';
import styles from '../Styles';
import {get,recupDonnee,getInfoUser,setToken, getProduitsRestaurant, getMenusRestaurant,getRestaurant} from '../API_Laravel';
import { DrawerItem } from '@react-navigation/drawer';
const URL = require('../URL');
export default class CarteRestaurant extends Component {
  state = {
    user: [],
  }  
  constructor(props) {
    super(props);
    const navObject =this.props.route.params;
    console.log(navObject);
    this.getData(navObject.idRestaurant);
    this.fadeAnimation = new Animated.Value(0);
  }

async getData(idRestaurant){
  var token = null
  while(token == null){
    token = await recupDonnee("@token");
  }
  setToken(token); // obligatoire !
  const menus = await getMenusRestaurant(token,idRestaurant);
  const produits = await getProduitsRestaurant(token,idRestaurant);
  const restaurant = await getRestaurant(token,idRestaurant);
  const user = await getInfoUser(token);
  const verif = 1;
  this.setState({verif});
  this.setState({produits});
  this.setState({menus});
  this.setState({restaurant});
  this.setState({token});
  this.setState({user});

}
  componentDidMount() {
    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }

  render() {
    const token = this.state.token;
    const verif = this.state.verif;
    const produits = this.state.produits;
    const menus = this.state.menus;
    const restaurant = this.state.restaurant;

    if(verif == 1 && produits != undefined && restaurant != undefined && menus != undefined){
      const produits = this.state.produits.produits;
      const nbProduits = this.state.produits.nbProduits;
      const restaurant = this.state.restaurant.restaurant;
      console.log(menus.nbMenus)
      if(menus.nbMenus != 0){
       console.log("ya au moins un menu")
      return (
      <View style={styles.container}>
      <FlatList
      data={menus.menus}
      keyExtractor={(item) => item.idMenu.toString()}
      renderItem={({item}) => 
      <TouchableOpacity style={styles.TouchableOpacityConnexion} onPress={()=>this.props.navigation.navigate('DetailMenu', {
        token: token,
        menu: item,})}>
        <Image  style={styles.tinyLogo} source={{uri: URL.URL + "images/menus/"+item.urlPhoto,}}/> 
      <Text style={styles.buttonConnexion}>{item.nomMenu}</Text>
      <Text style={styles.buttonConnexion}>{item.descriptionMenu}</Text>
    </TouchableOpacity>
     }
      />
     <FlatList
      data={produits.produits}
      keyExtractor={(item) => item.idProduit.toString()}
      renderItem={({item}) => 
      <TouchableOpacity style={styles.TouchableOpacityConnexion} onPress={()=>this.props.navigation.navigate('DetailMenu', {
        token: token,
        menu: item,})}>
        <Image  style={styles.tinyLogo} source={{uri: URL.URL + "images/menus/"+item.urlPhoto,}}/> 
      <Text style={styles.buttonConnexion}>{item.nom}</Text>
      <Text style={styles.buttonConnexion}>{item.description}</Text>
    </TouchableOpacity>
     }
      />
      </View>
      // produits
    );
  }else{ // pas de menus donc que produits
    return (
  
      <View style={styles.container}>
     <FlatList
      data={produits}
      keyExtractor={(item) => item.idProduit.toString()}
      renderItem={({item}) => 
      <TouchableOpacity style={styles.TouchableOpacityConnexion} onPress={()=>this.props.navigation.navigate('DetailProduit', {
        token: token,
        produit: item,})}>
        <Image  style={styles.tinyLogo} source={{uri: URL.URL + "images/produits/"+item.urlPhoto,}}/> 
      <Text style={styles.buttonConnexion}>{item.nom}</Text>
      <Text style={styles.buttonConnexion}>{item.description}</Text>
    </TouchableOpacity>
     }
      />
      
      </View>
      // produits
    );
    }
  
    }else{
      return(
        <View style={styles.containerLoading}>
        <ActivityIndicator size="large" color="#000000"/>
        </View>);
    }
  }
}
