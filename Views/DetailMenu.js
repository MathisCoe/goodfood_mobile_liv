import React, { Component, useState } from 'react';
import { Text, View, StyleSheet, Button,FlatList,Image, TouchableOpacity,ActivityIndicator, Animated} from 'react-native';
import styles from '../Styles';
const URL = require('../URL');
export default class DetailMenu extends Component {
  state = {
    menu: [],
  }  
    constructor(props) {
    super(props);
    const menu = this.props.route.params.menu
    this.setState({menu});
    this.fadeAnimation = new Animated.Value(0);
  }



  componentDidMount() {
    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }

  render() {
    console.log("render");
    console.log(this.props.route.params.menu);
    const menu = this.props.route.params.menu;
    if(menu != undefined){
    return (
    <View style={styles.container}>
      <Text style={styles.TextHaut}>Detail {menu.nomMenu} </Text>
      <FlatList
      data={menu.produits}
      keyExtractor={(item) => item.idProduit.toString()}
      renderItem={({item}) => 
      <TouchableOpacity style={styles.TouchableOpacityConnexion} onPress={()=>this.props.navigation.navigate('DetailProduit', {
        token: token,
        produit: item,})}>
        <Image  style={styles.tinyLogo} source={{uri: URL.URL + "images/produits/"+item.urlPhoto,}}/> 
      <Text style={styles.buttonConnexion}>{item.nom}</Text>
      <Text style={styles.buttonConnexion}>{item.description}</Text>
    </TouchableOpacity>
     }
      />
    </View>
    );
  }
  else{
    return(
      <View style={styles.containerLoading}>
      <ActivityIndicator size="large" color="#000000"/>
      </View>);
  }
  }

}
