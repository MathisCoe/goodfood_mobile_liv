import React, { Component } from 'react';
import 'react-native-gesture-handler';
import {StyleSheet,Text,View,Animated, SafeAreaView, ScrollView,TouchableOpacity,Button,TextInput,Image,Alert,Keyboard,ActivityIndicator} from 'react-native';
import styles from '../Styles';

import {recupDonnee,getInfoUser,setToken,getRestaurant,getProduitsRestaurant,getMenusRestaurant} from '../API_Laravel';


const restaurantitems = [{
  image: require('../assets/logo.png'),
  text: "Good Food",
},
];

export default class Accueil extends Component {

  state = {
    user: [],
    menu: [],
    restaurant: []
  }
  constructor(props) {
    super(props);
    this.getData();
    this.fadeAnimation = new Animated.Value(0);
  }


  async getData(){
    var token = null
    while(token == null){
      token = await recupDonnee("@token");
    }
    setToken(token); // obligatoire !
    const user = await getInfoUser(token);
	console.log("getRestaurant")
    const restaurant = await getRestaurant(token,user.restaurants_idRestaurant);
    console.log(restaurant);
    console.log("getProduitsRestaurant")
    const produits = await getProduitsRestaurant(token,user.restaurants_idRestaurant);
    console.log(produits);
    console.log("getMenuRestaurant")
    const menus = await getMenusRestaurant(token,user.restaurants_idRestaurant);
    console.log(menus);
    const verif = 1;
    this.setState({verif});
    this.setState({token});
    this.setState({user});
    this.setState({restaurant});
  }


  componentDidMount() {
    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }
  componentWillUnmount() {
    this.setState = (state,callback)=>{
      return;
    };
  }


  render() {
    // ATTENTION LE RENDER SE FAIT MEME SI ON A PAS ENCORE RECUP LES DONNEES DANS LA BDD
    // Ici on retrouve la carte du restaurant favori de l'user
    const token = this.state.token;
    const verif = this.state.verif;
    const restaurant = this.state.restaurant.restaurant;
    if(verif == 1 && restaurant != undefined){
      return (
    <SafeAreaView style={{ backgroundColor:"#E53825", flex: 1}}>
    <ScrollView showsVerticalScrollIndicator={false} style={{marginTop:10}}>
    <Text style={{backgroundColor:"#f39c12", flexDirection:"row", alignSelf:"center",fontSize: 20,
        fontWeight:"900", marginBottom:10, paddingVertical: 6, paddingHorizontal: 16}}>Restaurants</Text>
          <TouchableOpacity style={{backgroundColor:"#FFF", borderRadius:30, marginHorizontal:30}}>
            <View style={{flexDirection:"column", alignItems:"center"}}>
                <Image source={restaurantitems[0].image} style={styles.images_items} />
              <Text style={styles.Text}> {restaurant.nomRestaurant}</Text>
            </View>
        </TouchableOpacity>
        </ScrollView>
        </SafeAreaView>
      );
    }else{
      return(
        <View style={styles.containerLoading}>
        <ActivityIndicator size="large" color="#000000"/>
        </View>);
      }
    }

};

