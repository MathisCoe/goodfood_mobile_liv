import React, { Component, useState } from 'react';
import { Text, View, StyleSheet, Button, TouchableOpacity, Animated} from 'react-native';
import {Restart} from 'fiction-expo-restart';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from '../Styles';

export default class Deconnexion extends Component {
    constructor(props) {
    super(props);
    this.fadeAnimation = new Animated.Value(0);
  }

async deconnexion(){
    await AsyncStorage.removeItem('@token')
    await AsyncStorage.removeItem('@email')
    await AsyncStorage.removeItem('@password')
    await AsyncStorage.removeItem('@grade')
    Restart();
}

  componentDidMount() {
    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }

  render() {
    return (
    <View style={styles.container}>
      <Text style={styles.TextHaut}>Etes-vous sur de vouloir vous deconnecter ?</Text>
      <TouchableOpacity style={styles.TouchableOpacityTicketDeconnexion} onPress={()=>this.deconnexion()}>
        <Text style={styles.TextBouton}>Déconnexion</Text>
        </TouchableOpacity>
    </View>
    );
  }
}
