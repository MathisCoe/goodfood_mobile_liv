import React, { Component, useState } from 'react';
import { Text, View, StyleSheet, Button, TouchableOpacity, Animated} from 'react-native';
import styles from '../Styles';

export default class Panier extends Component {
    constructor(props) {
    super(props);
    this.fadeAnimation = new Animated.Value(0);
  }



  componentDidMount() {
    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }

  render() {
    return (
    <View style={styles.container}>
      <Text style={styles.TextHaut}>PAGE PANIER</Text>
    </View>
    );
  }
}
