import React, { Component, useState } from 'react';
import { Text, View, StyleSheet, Button, TouchableOpacity, Animated,ActivityIndicator,FlatList} from 'react-native';
import styles from '../Styles';
import {recupDonnee,getInfoUser,setToken,get} from '../API_Laravel';
export default class HistoriqueCommandes extends Component {
  state = {
    user: [],
    menu: [],
  }
    constructor(props) {
    super(props);
    this.getData();
    this.fadeAnimation = new Animated.Value(0);
  }
async getData(){
  var token = null
  while(token == null){
    token = await recupDonnee("@token");
  }
  setToken(token); // obligatoire !
  const user = await getInfoUser(token);
  const historiqueCommandes = await get(token,"user/commandes");
  console.log(historiqueCommandes)
  const verif = 1;
  this.setState({historiqueCommandes});
  this.setState({verif});
  this.setState({token});
  this.setState({user});

}

  componentDidMount() {
    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }

  render() {
    // ATTENTION LE RENDER SE FAIT MEME SI ON A PAS ENCORE RECUP LES DONNEES DANS LA BDD
    const token = this.state.token;
    const verif = this.state.verif;
    const user = this.state.user;
    const historiqueCommandes = this.state.historiqueCommandes;
    if(verif == 1 && historiqueCommandes != undefined){
      console.log(historiqueCommandes);
      return (
        <View style={styles.container}>
              <Text style={styles.Text}> Historique des Commandes </Text>
              <FlatList
      data={historiqueCommandes.commandes}
      keyExtractor={(item) => item.idCommande.toString()}
      renderItem={({item}) =>
      <Text style={styles.buttonConnexion}>Commande N°{item.idCommande}</Text>
     }
      />
        </View>
      );
    }else{
      return(
        <View style={styles.containerLoading}>
        <ActivityIndicator size="large" color="#000000"/>
        </View>);
      }
  }
}
