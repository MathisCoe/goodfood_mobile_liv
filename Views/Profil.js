import React, { Component, useState } from 'react';
import { Text, View,Image, StyleSheet, Button, TouchableOpacity, Animated,ActivityIndicator} from 'react-native';
import {Restart} from 'fiction-expo-restart';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from '../Styles';
import {recupDonnee,getInfoUser,setToken} from '../API_Laravel';

export default class Profil extends Component {
  state = {
    user: [],
    menu: [],
  }
    constructor(props) {
    super(props);
    this.getData();
    this.fadeAnimation = new Animated.Value(0);
  }

  async deconnexion(){
    console.log("Déconnexion")
    await AsyncStorage.removeItem('@token')
    await AsyncStorage.removeItem('@email')
    await AsyncStorage.removeItem('@password')
    Restart();
}

async getData(){
  var token = null
  while(token == null){
    token = await recupDonnee("@token");
  }
  setToken(token); // obligatoire !
  const user = await getInfoUser(token);
  const verif = 1;
  this.setState({verif});
  this.setState({token});
  this.setState({user});

}

  componentDidMount() {
    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }

  render() {
    const token = this.state.token;
    const verif = this.state.verif;
    const user = this.state.user;
    // ATTENTION LE RENDER SE FAIT MEME SI ON A PAS ENCORE RECUP LES DONNEES DANS LA BDD DONC ON CHECK
    if(verif == 1 && user != undefined){
      return (
        <View style={styles.container}>
          <Image style={{width:150, height:150, borderRadius:100}}
        source={require("../assets/kendall.png")}></Image>
          <Text style={styles.Text}> {user.prenom} {user.nom}</Text>
          
          <TouchableOpacity style={styles.TouchableOpacityConnexion} onPress={()=>this.props.navigation.navigate('HistoriqueCommandes', {
              token: token,})}>
            <Text style={styles.buttonConnexion}>Historique des Commandes</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.TouchableOpacityConnexion} onPress={() =>this.deconnexion()}>
            <Text style={styles.buttonConnexion}>Deconnexion</Text>
          </TouchableOpacity>
        </View>
      );
    }else{
      return(
        <View style={styles.containerLoading}>
            <ActivityIndicator size="large" color="#000000"/>
        </View>);
      }
  }
}
