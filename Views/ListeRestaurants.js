import React, { Component, useState } from 'react';
import { Text, View, StyleSheet, Button, TouchableOpacity,FlatList, Animated,ActivityIndicator,Image} from 'react-native';
import styles from '../Styles';
import {get,recupDonnee,getInfoUser,setToken} from '../API_Laravel';
const URL = require('../URL');

export default class ListeRestaurants extends Component {
  state = {
    user: [],
  }  
  constructor(props) {
    super(props);
    this.getData();
    this.fadeAnimation = new Animated.Value(0);
  }

async getData(){
  var token = null
  while(token == null){
    token = await recupDonnee("@token");
  }
  setToken(token); // obligatoire !
  const restaurants = await get(token,"restaurant/departementUser");
  console.log(restaurants);
  const user = await getInfoUser(token);
  const verif = 1;
  this.setState({verif});
  this.setState({restaurants});
  this.setState({token});
  this.setState({user});

}
  componentDidMount() {
    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }

  render() {
    const token = this.state.token;
    const verif = this.state.verif;
    const restaurants = this.state.restaurants;
   // const nbRestaurants = this.state.restaurants.nbRestaurants;

    if(verif == 1 && restaurants != undefined){
      const restaurants = this.state.restaurants.restaurants;
      const nbRestaurants = this.state.restaurants.nbRestaurants;
      console.log(restaurants)
    return (
    <View style={styles.container}>
      <FlatList
      data={restaurants}
      keyExtractor={(item) => item.idRestaurant.toString()}
      renderItem={({item}) => 
      <TouchableOpacity style={styles.TouchableOpacityConnexion} onPress={()=>this.props.navigation.navigate('CarteRestaurant', {
        token: token,
        idRestaurant: item.idRestaurant,})}>
        <Image  style={styles.tinyLogo} source={{uri: URL.URL + "images/restaurants/"+item.urlPhoto,}}/> 
      <Text style={styles.buttonConnexion}>{item.nomRestaurant}</Text>
    </TouchableOpacity>
     }
      />
      </View>
    );
    }else{
      return(
        <View style={styles.containerLoading}>
        <ActivityIndicator size="large" color="#000000"/>
        </View>);
    }
  }
}
