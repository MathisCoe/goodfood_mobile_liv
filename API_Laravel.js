import Axios from './Axios.js';
import AsyncStorage from '@react-native-async-storage/async-storage';
export async function stockerDonnee(key,data) {
  try {
    const jsonValue = await AsyncStorage.getItem(key)
    if(jsonValue == null) {
      await AsyncStorage.setItem(key, JSON.stringify(data))
    }
  } catch(e) {
    console.log('ERREUR : ' + e);
  }
}
export async function recupDonnee(key) {
  try {
    const value = await AsyncStorage.getItem(key)
    if(value !== null) {
      return JSON.parse(value);
    } else {
      return null;
    }
  } catch(e) {
  }
}
export function setToken(token){
  Axios.defaults.headers.common = {
    'Authorization': 'Bearer ' + token
};
}
export function getToken(email,password){
  const token = Axios.post('authenticate', {
    email: email,
    password: password
  })
  .then(res =>{
    const token = res.data.token;
    return res;
  })
  return token;

}
export function get(token,cheminAPI){
  const data = Axios.post(cheminAPI, {
    token: token
  }).then(res =>{
    const data = res.data;
    return data;
  })
  return data;
}
export function getInfoUser(token){
  const data = Axios.post("user", {
    token: token
  }).then(res =>{
    const data = res.data;
    return data;
  })
  return data;
}

export function getMenu(token,idMenu){
  const data = Axios.post("menu", {
    token: token,
    idMenu:idMenu
  }).then(res =>{
    const data = res.data;
    return data;
  })
  return data;
}

export function getProduit(token,idProduit){
  const data = Axios.post("produit", {
    token: token,
    idProduit:idProduit
  }).then(res =>{
    const data = res.data;
    return data;
  })
  return data;
}
export function getRestaurant(token,idRestaurant){
  const data = Axios.post("restaurant", {
    token: token,
    idRestaurant:idRestaurant
  }).then(res =>{
    const data = res.data;
    return data;
  })
  return data;
}
export function getMenusRestaurant(token,idRestaurant){
  console.log(idRestaurant);
  const data = Axios.post("restaurant/menus", {
    token: token,
    idRestaurant:idRestaurant
  }).then(res =>{
    const data = res.data;
    return data;
  })
  return data;
}
export function getProduitsRestaurant(token,idRestaurant){
  const data = Axios.post("restaurant/produits", {
    token: token,
    idRestaurant:idRestaurant
  }).then(res =>{
    const data = res.data;
    return data;
  })
  return data;
}

