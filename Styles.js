import {StyleSheet,Dimensions} from 'react-native';
const {height, width} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    backgroundColor: '#E53825',
    justifyContent:'center',
    height: height,
    width: width
  },
  spinnerTextStyle: {
    color: '#FFF'
  },
  tinyLogo: {
    width: 200,
    height: 150,
    justifyContent:'center',
    alignItems:'center',
  },
  WeeklyCalendar:{
    flex: 3,
  },
  inputBox:
  {
    width:300,
    height:50,
    backgroundColor: "white",
    borderRadius:25,
    paddingHorizontal: 16,
    fontSize:16,
    marginVertical:10,
  },
  TouchableOpacityConnexion:
  {
    width:300,
    backgroundColor:'#ecf0f1',
    borderRadius:15,
    marginVertical:10,
    paddingVertical:12,
  },
  buttonConnexion:
  {
    textAlign:'center',
    fontSize : 16,
    fontWeight: '500',
  },
  logoText:
  {
    fontSize:18,
    color:'rgba(0,0,0,0.7)'
  },
  containerLoading: {
    flex: 1,
    alignItems:'center',
    backgroundColor: '#E53825',
    justifyContent:'center',
  },
  Text: {
    fontSize : 20,
    marginTop:10,
    marginBottom:10,
    fontWeight: 'bold',
  },
  TouchableOpacityAdd:
  {
    width:50,
    height:30,
    backgroundColor: "white",
    borderRadius:25,
    paddingHorizontal: 16,
    fontSize:16,
    marginVertical:10,
  },

  TouchableOpacityNext:
  {
    width:50,
    height:30,
    backgroundColor: "white",
    borderRadius:25,
    paddingHorizontal: 16,
    fontSize:16,
    marginVertical:10,
  },

  buttonNext:
  {
    fontSize : 20,
    fontWeight: 'bold',
    paddingHorizontal:1,
  },
  TextInputTicket:
  {
    width:width * 0.95,
    height:50,
    backgroundColor: "white",
    borderRadius:10,
    paddingHorizontal: 12,
    paddingVertical:12,
    fontSize:16,
    marginVertical:10,

  },
  TextInputTicketDescription:
  {
    width:width * 0.95,
    height:150,
    backgroundColor: "white",
    borderRadius:10,
    paddingHorizontal: 12,
    paddingVertical:12,
    fontSize:16,
    marginVertical:10,

  },
  valider: {
    flexDirection: 'row',
    backgroundColor: '#FFC853',
    justifyContent: 'space-between',

  },TouchableOpacityValider:
  {
    width:100,
    height:50,
    backgroundColor: "#4cd137",
    borderRadius:10,
    paddingHorizontal: 25,
    paddingVertical:15,
    fontSize:16,
    marginVertical:10,
  },
  TouchableOpacityRefuser:
  {
    width:100,
    height:50,
    backgroundColor: "#EA2027",
    borderRadius:10,
    paddingHorizontal: 22,
    paddingVertical:15,
    fontSize:16,
    marginVertical:10,
  },
  TouchableOpacityAnnuler:
  {
    width:100,
    height:50,
    backgroundColor: "#b2bec3",
    borderRadius:10,
    paddingHorizontal: 22,
    paddingVertical:15,
    fontSize:16,
    marginVertical:10,
  },
  TextInputDescription:
  {
    width:325,
    height:120,
    backgroundColor: "white",
    borderRadius:10,
    paddingHorizontal: 12,
    fontSize:16,

  },
  TouchableOpacityFermerMateriel:
  {
    width:90,
    height:50,
    backgroundColor: "#b2bec3",
    borderRadius:10,
    paddingHorizontal: 22,
    paddingVertical:15,
    fontSize:16,
    marginVertical:10,
  },
  TouchableOpacityAjouterMateriel:
  {
    width:170,
    height:50,
    backgroundColor: "#b2bec3",
    borderRadius:10,
    paddingHorizontal: 22,
    paddingVertical:15,
    fontSize:16,
    marginVertical:10,
  },
  TouchableOpacityListeApprenants:
  {
    width:220,
    height:50,
    backgroundColor: "#b2bec3",
    borderRadius:10,
    paddingHorizontal: 22,
    paddingVertical:15,
    fontSize:16,
    marginVertical:10,
  },
  TouchableOpacityVoirMateriel:
  {
    width:230,
    height:50,
    backgroundColor: "#b2bec3",
    borderRadius:10,
    paddingHorizontal: 22,
    paddingVertical:15,
    fontSize:16,
    marginVertical:10,
  },
  TouchableOpacityVoirApprenants:
  {
    width:240,
    height:50,
    backgroundColor: "#b2bec3",
    borderRadius:10,
    paddingHorizontal: 22,
    paddingVertical:15,
    fontSize:16,
    marginVertical:10,
  },
  item: {
    backgroundColor: '#ffffff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 15,
  },
  buttonTicket:
  {
    fontSize : 16,
    fontWeight: '500',
  },
  buttonTicketRed:
  {
    fontSize : 16,
    fontWeight: '500',
    color: 'red',
  },
  buttonTicketOrange:
  {
    fontSize : 16,
    fontWeight: '500',
    color: 'orange'
  },
  buttonTicketGreen:
  {
    fontSize : 16,
    fontWeight: '500',
    color: 'green'
  },
  styleTexte:
  {
    fontSize : 18,
    fontWeight: 'bold',
  },
  TextHaut: {
    fontSize : 20,
    marginTop:1,
    marginHorizontal: 20,
    marginBottom:10,
    fontWeight: 'bold',
  },
  TextBouton: {
    fontSize : 20,
    marginTop:1,
    marginBottom:10,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  accueil_touchable: {
    backgroundColor:'#ff7675',
    borderRadius:10,
  },
  images_items: {
    resizeMode: 'stretch',
    borderRadius: 50,
},
});
